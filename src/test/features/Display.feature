# language: ca

#noinspection SpellCheckingInspection
Característica: Comprovació del funcionament del display.

  Rerefons:
    Donada la joguina del catàleg "lego" amb codi de barres "8810ECB" i preu "12.0" euros sense IVA i un IVA del "21"%
    I la joguina del catàleg "ninja turtle" amb codi de barres "0334SA3" i preu "20.3" euros sense IVA i un IVA del "21"%
    I la joguina del catàleg "power ranger" amb codi de barres "D04MSD3" i preu "18.8" euros sense IVA i un IVA del "21"%
    I la joguina del catàleg "baldufa" amb codi de barres "CS04WD4" i preu "3.5" euros sense IVA i un IVA del "21"%
    I la joguina del catàleg "Wii U" amb codi de barres "034MCA3" i preu "349.9" euros sense IVA i un IVA del "21"%
    I la joguina del catàleg "monstre bu" amb codi de barres "62XMVFM" i preu "30.0" euros sense IVA i un IVA del "21"%
    I la joguina del catàleg "x-box" amb codi de barres "546SSMG" i preu "299.9" euros sense IVA i un IVA del "21"%
    I la joguina del catàleg "super mario" amb codi de barres "235450C" i preu "59.5" euros sense IVA i un IVA del "21"%
    I la joguina del catàleg "barbie" amb codi de barres "213450V" i preu "28.0" euros sense IVA i un IVA del "21"%
    I la joguina del catàleg "cocolin pis pis" amb codi de barres "DCM3420" i preu "54.7" euros sense IVA i un IVA del "21"%

  Escenari: Comprovar el funcionament del display
    Donat un client que passa per caixa per obrir una venta
    Quan passo la joguina amb codi de barres "CS04WD4"
    Aleshores el display mostra "1 x baldufa" breument
    I mostra el total acumulat de la venta amb IVA: "Total 4,235€"
    Aleshores passo la joguina amb codi de barres "D04MSD3" amb una quantitat de 2
    Aleshores el display mostra "2 x power ranger" breument
    I mostra el total acumulat de la venta amb IVA: "Total 49,731€"
    Aleshores elimino de la venta la joguina amb codi de barres "D04MSD3"
    Aleshores el display mostra "- 2 x power ranger" breument
    I mostra el total acumulat de la venta amb IVA: "Total 4,235€"
    Aleshores el preu total de la venta actual és de "3.5" euros sense IVA
    I "4.235" euros amb IVA
    Quan el client paga "10.0" euros en "efectiu"
    Aleshores es tanca la venta actual pagada en "efectiu" amb 1 linias de venta i un preu de "3.5" euros sense IVA i "4.235" euros amb IVA
    I el canvi a retornar es de "5.765" euros