package edu.upc.essi.gps.ecommerce;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import edu.upc.essi.gps.domain.JogueteriaException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class StepDefinitions {

    private ToysRepository toysRepository = new ToysRepository();
    private ToysService toysService = new ToysService(toysRepository);
    private Exception ex;

    /***************************************SALES***********************************************/

    @Given ("^la joguina del catàleg \"([^\"]*)\" amb codi de barres \"([^\"]*)\" i preu \"([^\"]*)\" euros sense IVA i un IVA del \"([^\"]*)\"%$")
    public void I_create_a_toy_in_Repository(String toyName, String barCode, String price, String iva) {
        toysService.newToy(toyName, barCode, parsePriceToFloat(price),parseIVAToFloat(iva));
    }

    @Given ("^un client que passa per caixa per obrir una venta$")
    public void I_create_a_new_sale() {
        toysService.newSale();
    }

    @When ("^passo la joguina amb codi de barres \"([^\"]*)\" amb una quantitat de (\\d+)$")
    public void I_insert_sales_line_with_quantity_in_sale(String barCode, int quantity) {
        try {
            toysService.addSaleLine(barCode,quantity);
        }
        catch (JogueteriaException jErr){
            this.ex = jErr;
        }
    }

    @When ("^passo la joguina amb codi de barres \"([^\"]*)\"$")
    public void I_insert_sales_line_without_quantity_in_sale(String barCode) {
        try {
            toysService.addSaleLine(barCode);
        }
        catch (JogueteriaException jErr){
            this.ex = jErr;
        }
    }

    @Then ("^el preu total de la venta actual és de \"([^\"]*)\" euros sense IVA$")
    public void I_check_sale_price(String price) {
        assertEquals(parsePriceToFloat(price), toysService.getSaleNotClosed().getPrice(), parsePriceToFloat(price));
    }

    @And ("^\"([^\"]*)\" euros amb IVA$")
    public void I_check_sale_price_with_IVA (String priceWithIVA) {
        assertEquals(parsePriceToFloat(priceWithIVA), toysService.getSaleNotClosed().getPriceWithIVA(), parsePriceToFloat(priceWithIVA));
    }

    @When ("^el client paga en \"([^\"]*)\"$")
    public void collect_payment_for_close_sale(String paymentType) {
        toysService.setSalePaymentType(convertPaymentTypeToSysData(paymentType));
    }

    @When ("^el client paga \"([^\"]*)\" euros en \"([^\"]*)\"$")
    public void collect_payment_for_close_sale(String payment, String paymentType) {
        toysService.setLastClientPayment(parsePriceToFloat(payment));
        toysService.setSalePaymentType(convertPaymentTypeToSysData(paymentType));
    }

    @Then ("^es tanca la venta actual pagada en \"([^\"]*)\" amb (\\d+) linias de venta i un preu de \"([^\"]*)\" euros sense IVA$")
    public void I_close_and_check_closed_sale(String paymentType, int saleSize, String price) {
        toysService.confirmSale();
        assertEquals(paymentType, this.convertPaymentTypeToPresData(this.toysService.getSale().getPaymentType()));
        assertEquals(saleSize, toysService.getSale().getSaleLinesSize());
        assertEquals(parsePriceToFloat(price),toysService.getSale().getPrice(),parsePriceToFloat(price));
    }

    @Then ("^es tanca la venta actual pagada en \"([^\"]*)\" amb (\\d+) linias de venta i un preu de \"([^\"]*)\" euros sense IVA i \"([^\"]*)\" euros amb IVA$")
    public void I_close_and_check_closed_sale(String paymentType, int saleSize, String price, String priceWithIVA) {
        toysService.confirmSale();
        assertEquals(paymentType, this.convertPaymentTypeToPresData(this.toysService.getSale().getPaymentType()));
        assertEquals(saleSize, toysService.getSale().getSaleLinesSize());
        assertEquals(parsePriceToFloat(price),toysService.getSale().getPrice(),parsePriceToFloat(price));
        assertEquals(parsePriceToFloat(priceWithIVA),toysService.getSale().getPrice(),parsePriceToFloat(priceWithIVA));
     }

    @Then ("^el canvi a retornar es de \"([^\"]*)\" euros$")
    public void I_return_client_change (String change) {
        assertEquals(parsePriceToFloat(change), toysService.getLastSaleChange(),parsePriceToFloat(change));
    }

    @And("^actualitzo les unitats de la joguina \"([^\"]*)\" a (\\d+)$")
    public void I_update_salesLine_quantity_after(String barCode, int quantity) {
        try {
            toysService.setSaleLineQuantity(barCode, quantity);
        } catch (JogueteriaException jErr) {
            this.ex = jErr;
        }
    }

    @And ("^elimino de la venta la joguina amb codi de barres \"([^\"]*)\"$")
    public void delete_sale_line(String barCode) {
        try {
            toysService.deleteSaleLine(barCode);
        }
        catch (JogueteriaException jErr) {
            this.ex = jErr;
        }
    }

    @Then("^obtinc el següent missatge d'error \"([^\"]*)\"$")
    public void there_is_a_failure_reporting(String msg) {
        assert (this.ex!= null);
        assertEquals(msg, this.ex.getMessage());
    }

    @Then("^el display mostra \"([^\"]*)\" breument$")
    public void Display_show_SaleLine(String msg) {
        assertEquals(msg, toysService.getDisplayLastSaleLineMsg());
    }

    @And("^mostra el total acumulat de la venta amb IVA: \"([^\"]*)\"$")
    public void Display_show_not_closed_sale_total_price(String msg) {
        assertEquals(msg, toysService.getDisplayLastSalePriceMsg());
    }

    private int convertPaymentTypeToSysData(String paymentType) {
        if (paymentType.equals("efectiu")) return 1;
        else return 0;
    }

    private String convertPaymentTypeToPresData(int paymentType) {
        if (paymentType == 1) return "efectiu";
        else return "No pagada";
    }

    private float parsePriceToFloat (String price) {
        return Float.parseFloat(price);
    }

    private float parseIVAToFloat (String iva) {
        return Float.parseFloat(iva)/(float)100;
    }
}
