package edu.upc.essi.gps.utils;

import edu.upc.essi.gps.domain.Entity;
import edu.upc.essi.gps.domain.HasName;
import edu.upc.essi.gps.domain.HasBarCode;

import java.util.Comparator;

public final class Comparators {

    private Comparators() {
    }

    public static Comparator<HasName> byName = new Comparator<HasName>() {

        @Override
        public int compare(HasName o1, HasName o2) {
            return o1.getName().compareTo(o2.getName());
        }

    };

    public static Comparator<HasBarCode> byBarCode = new Comparator<HasBarCode>() {

        @Override
        public int compare(HasBarCode o1, HasBarCode o2) {
            return o1.getBarCode().compareTo(o2.getBarCode());
        }

    };

    public static Comparator<Entity> byId = new Comparator<Entity>() {
        @Override
        public int compare(Entity o1, Entity o2) {
            return new Long(o1.getId()).compareTo(o2.getId());
        }
    };
}
