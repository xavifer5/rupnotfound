package edu.upc.essi.gps.utils;

import edu.upc.essi.gps.domain.HasBarCode;
import edu.upc.essi.gps.domain.HasName;

public final class Matchers {

    private Matchers() {
    }

    public static Matcher<HasName> nameMatcher(final String name) {
        return new Matcher<HasName>() {
            @Override
            public boolean matches(HasName named) {
                return named.getName().equals(name);
            }
        };
    }

    public static Matcher<HasBarCode> barCodeMatcher(final String barCode) {
        return new Matcher<HasBarCode>() {
            @Override
            public boolean matches(HasBarCode barCoded) {
                return barCoded.getBarCode().equals(barCode);
            }
        };
    }

    public static Matcher<Object> all = new Matcher<Object>() {
        @Override
        public boolean matches(Object entity) {
            return true;
        }
    };
}



