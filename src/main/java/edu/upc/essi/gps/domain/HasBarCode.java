package edu.upc.essi.gps.domain;

/**
 * Created by Victor on 02/12/2014.
 */
public interface HasBarCode {
    public String getBarCode();
}
