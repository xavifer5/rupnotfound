package edu.upc.essi.gps.domain;

/**
 * Created by Victor on 02/12/2014.
 */
public class JogueteriaException extends Exception {
    public JogueteriaException(String message) {
        super(message);
    }
}
