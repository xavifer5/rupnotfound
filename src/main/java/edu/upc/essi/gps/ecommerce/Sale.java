package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.domain.JogueteriaException;

import java.util.ArrayList;

/**
 * Created by Victor on 01/12/2014.
 */
public class Sale {
    private ArrayList<SalesLine> saleLines;
    private float price;
    private float priceWithIVA;
    private int paymentType; //0: No pagada, 1:Efectiu, 2:Targeta, 3:Val de descompte
    private int nextSalesLineId;

    public Sale (long id) {
        saleLines = new ArrayList<SalesLine>();
        this.price = 0;
        this.priceWithIVA = 0;
        nextSalesLineId = 1;
    }

    public float getPrice() {
        return this.price;
    }

    public float getPriceWithIVA() {
        return this.priceWithIVA;
    }

    public int getPaymentType() {
        return this.paymentType;
    }

    public void setPaymentType(int paymentType) {
        this.paymentType = paymentType;
    }

    public int getNextSalesLineId() {
        return this.nextSalesLineId;
    }

    public int getSaleLinesSize() {
        return this.saleLines.size();
    }

    public void addSalesLine(Toy toy, int quantity) throws JogueteriaException {
       try {
           SalesLine l = new SalesLine(toy, quantity);
           this.saleLines.add(l);
           ++nextSalesLineId;
           this.price += l.getPrice();
           this.priceWithIVA += l.getPriceWithIVA();
       }
       catch (JogueteriaException jErr) {
           throw new JogueteriaException(jErr.getMessage());
       }
    }

    public void addSalesLine(Toy toy) {
        SalesLine l = new SalesLine(toy);
        this.saleLines.add(l);
        ++nextSalesLineId;
        this.price += l.getPrice();
        this.priceWithIVA += l.getPriceWithIVA();
    }

    public void updateSaleLine(int index, int quantity) throws JogueteriaException {
        SalesLine l = saleLines.get(index);
        float oldPrice = l.getPrice();
        float oldPriceWithIVA = l.getPriceWithIVA();
        l.setQuantity(quantity);
        //Si no hi ha hagut excepció acabo el procès.
        this.price -= oldPrice;
        this.priceWithIVA -= oldPriceWithIVA;
        this.price += l.getPrice();
        this.priceWithIVA += l.getPriceWithIVA();
    }

    public void deleteSaleLine(int index) {
        SalesLine l = saleLines.get(index);
        float slPrice = l.getPrice();
        float slPriceWithIVA = l.getPriceWithIVA();
        this.saleLines.remove(index);
        this.price -= slPrice;
        this.priceWithIVA -= slPriceWithIVA;
    }

    public SalesLine getSaleLine(int index) {
        return saleLines.get(index);
    }

    public int getSaleLineIndex(String barCode) {
        for (int i = 0; i < getSaleLinesSize(); ++i) {
            if (saleLines.get(i).getToy().getBarCode().equals(barCode)) return i;
        }
        return -1;
    }
}
