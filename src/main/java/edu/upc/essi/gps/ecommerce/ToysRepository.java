package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.utils.Matchers;
import edu.upc.essi.gps.utils.Repository;

/**
 * Created by Victor on 02/12/2014.
 */
public class ToysRepository extends Repository<Toy> {

    public Toy findByName(final String name) {
        return find(Matchers.nameMatcher(name));
    }

    public Toy findByBarCode(final String barCode) {
        return find(Matchers.barCodeMatcher(barCode));
    }

    @Override
    protected void checkInsert(Toy entity) throws RuntimeException {
        if (findByName(entity.getName()) != null)
            throw new IllegalArgumentException("Ja existeix una joguina amb aquest nom");
    }
}