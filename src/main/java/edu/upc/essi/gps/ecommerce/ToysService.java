package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.domain.JogueteriaException;

/**
 * Created by Victor on 02/12/2014.
 */
public class ToysService {
    private ToysRepository ToysRepository;
    private Sale sale;
    private Sale saleNotClosed;
    private float lastSalePayment;
    private String displayLastSaleLineMsg;

    public ToysService(ToysRepository ToysRepository) {
        this.ToysRepository = ToysRepository;
    }

    public void newToy(String name, String barCode, float price, float iva){
        long id = ToysRepository.newId();
        Toy result = new Toy(id,name,barCode,price,iva);
        ToysRepository.insert(result);
    }

    /**********************************TOY***********************************/

    public Toy findByBarCode(String barCode) {
        return ToysRepository.findByBarCode(barCode);
    }

    /**********************************SALE***********************************/

    public void newSale() {
        saleNotClosed = new Sale(1);
    }

    public Sale getSale() {
        return this.sale;
    }

    public Sale getSaleNotClosed() {
        return this.saleNotClosed;
    }

    public void addSaleLine(String barCode, int quantity) throws JogueteriaException {
        int index = saleNotClosed.getSaleLineIndex(barCode);
        if (index == -1) {
            Toy t = findByBarCode(barCode);
            saleNotClosed.addSalesLine(t, quantity);
            setDisplayLastSaleLineMsg(true,quantity,t.getName());
        }
        else {
            int newQuantity = saleNotClosed.getSaleLine(index).getQuantity() + quantity;
            saleNotClosed.updateSaleLine(index,newQuantity);
            setDisplayLastSaleLineMsg(true,newQuantity,saleNotClosed.getSaleLine(index).getToy().getName());
        }
    }

    public void addSaleLine(String barCode) throws JogueteriaException {
        int index = saleNotClosed.getSaleLineIndex(barCode);
        if (index == -1) {
            Toy t = findByBarCode(barCode);
            saleNotClosed.addSalesLine(t);
            setDisplayLastSaleLineMsg(true,1,t.getName());
        }
        else {
            int newQuantity = saleNotClosed.getSaleLine(index).getQuantity() + 1;
            saleNotClosed.updateSaleLine(index,newQuantity);
            setDisplayLastSaleLineMsg(true,newQuantity,saleNotClosed.getSaleLine(index).getToy().getName());
        }
    }

    public void setSalePaymentType(int paymentType) {
        saleNotClosed.setPaymentType(paymentType);
    }

    public void setSaleLineQuantity(String barCode, int quantity) throws JogueteriaException {
        int index = saleNotClosed.getSaleLineIndex(barCode);
        if (index == -1) throw new JogueteriaException("La joguina amb el codi de barres " + barCode + " no existeix en aquesta venta");
        saleNotClosed.updateSaleLine(index, quantity);
        setDisplayLastSaleLineMsg(true,quantity,saleNotClosed.getSaleLine(index).getToy().getName());
    }

    public void deleteSaleLine (String barCode) throws JogueteriaException{
        int index = saleNotClosed.getSaleLineIndex(barCode);
        if (index == -1) throw new JogueteriaException("La joguina amb el codi de barres " + barCode + " no existeix en aquesta venta");
        setDisplayLastSaleLineMsg(false,saleNotClosed.getSaleLine(index).getQuantity(),saleNotClosed.getSaleLine(index).getToy().getName());
        saleNotClosed.deleteSaleLine(index);
    }

    public void confirmSale() {
        this.sale = this.saleNotClosed;
    }

    public void setLastClientPayment(float payment) {
        this.lastSalePayment = payment;
    }

    public float getLastSaleChange() {
        return lastSalePayment - sale.getPriceWithIVA();
    }

    public String getDisplayLastSaleLineMsg() {
        return this.displayLastSaleLineMsg;
    }

    public void setDisplayLastSaleLineMsg(boolean addOrRemove, int quantity, String toyName) {
        displayLastSaleLineMsg = "";
        if (!addOrRemove) displayLastSaleLineMsg = displayLastSaleLineMsg + "- ";
        displayLastSaleLineMsg = displayLastSaleLineMsg + String.valueOf(quantity) + " x " + toyName;
    }

    public String getDisplayLastSalePriceMsg() {
        return "Total " + String.format("%.3f",this.saleNotClosed.getPriceWithIVA()) + "€";
    }
}
