package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.domain.JogueteriaException;

/**
 * Created by Victor on 01/12/2014.
 */
public class SalesLine {
    Toy j;
    int quantity;
    float price;
    float priceWithIVA;

    public SalesLine(Toy j, int quantity) throws JogueteriaException{
        this.j = j;
        if (quantity > 0) this.quantity = quantity;
        else throw new JogueteriaException("La quantitat no pot ser menor o igual a 0");
        this.price = j.getPrice()*quantity;
        this.priceWithIVA = j.getPriceWithIVA()*quantity;
    }

    public SalesLine(Toy j) {
        this.j = j;
        this.quantity = 1;
        this.price = j.getPrice()*quantity;
        this.priceWithIVA = j.getPriceWithIVA()*quantity;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) throws JogueteriaException {
        if (quantity <= 0) throw new JogueteriaException("La quantitat no pot ser menor o igual a 0");
        this.quantity = quantity;
        this.price = j.getPrice()*quantity;
        this.priceWithIVA = j.getPriceWithIVA()*quantity;
    }

    public float getPrice() {
        return this.price;
    }

    public float getPriceWithIVA() {
        return this.priceWithIVA;
    }

    public Toy getToy() {
        return this.j;
    }
}
