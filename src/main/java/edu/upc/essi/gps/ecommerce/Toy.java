package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.domain.Entity;
import edu.upc.essi.gps.domain.HasBarCode;
import edu.upc.essi.gps.domain.HasName;

/**
 * Created by Victor on 01/12/2014.
 */

public class Toy implements Entity, HasName, HasBarCode {
    private long id;
    private String name,barCode;
    private float price;
    private float iva;

    public Toy(long id, String name, String barCode, float price, float iva) {
        this.id = id;
        this.barCode = barCode;
        this.name = name;
        this.price = price;
        this.iva = iva;
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getBarCode() {
        return this.barCode;
    }

    public float getPrice() {
        return this.price;
    }

    public float getPriceWithIVA() {
        return this.price += this.price*this.iva;
    }
}
