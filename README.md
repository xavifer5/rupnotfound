# GPS

Projecte de partida per al laboratori de GPS de la FIB, curs de tardor de 2014.

### Crear el repositori del vostre grup

1. Seleccioneu l'opció Fork del menú de l'esquerra (de vegades "amagada" dins la icona dels punts suspensius).
2. Poseu el vostre nom de grup com a nom del repositori
3. Marqueu el repositori com a privat
4. Premeu acceptar
5. Editeu aquest fitxer README.md (es pot fer des del propi Bitbucket) per a incloure la llista de components del grup enlloc d'aquestes instruccions.